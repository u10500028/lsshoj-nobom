<script>
function askAccount(){
	var ask = prompt("<?php
		if(isset($_GET['message'])) echo $_GET['message'];
		else echo "LSSHOJ沒有你的帳號,請輸入你想要的帳號,我幫你變出來";
		?> ");
	if(ask == null || ask == ""){
		window.location.href = "./google.php?message=你到底要不要用啦?";
	}
	else{
		window.location.href = "./google.php?account="+ask;
	}
}
<?php
	require_once("../../include/db_info.inc.php");
	require_once './Google/Google_Client.php';
	require_once './Google/contrib/Google_Oauth2Service.php';
	session_start();
	ini_set("display_errors","Off");
	
	//Start Oauth
	//建立apiClient
	$client = new Google_Client();

	//建立Oauth2 Service
	$oauth2 = new Google_Oauth2Service($client);

	//處理回傳的code
	if (isset($_GET['code'])) {
	  $client->authenticate();
	  $_SESSION['token'] = $client->getAccessToken();
	  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
	  exit;
	}
	//設定Token
	if (isset($_SESSION['token'])) {
	 $client->setAccessToken($_SESSION['token']);
	}

	//取消Session
	if (isset($_REQUEST['logout'])) {
	  unset($_SESSION['token']);
	  $client->revokeToken();
	}

	if ($client->getAccessToken()) {
		$user = $oauth2->userinfo->get();
		//Start to Auth and Login
		$email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
		
		//If logged in it means only have to link
		if(isset($_SESSION['user_id'])) require_once("./link.php");
		else require_once("./regist.php");

		//Well Done You're in~~
		header("Location: ../../");
		exit;
	} 
	else{
		$authUrl = $client->createAuthUrl();
		header("Location: ".$authUrl);
		exit;
	}
?>
</script>