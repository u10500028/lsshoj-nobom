<?php
	chdir("..");
 $cache_time=0; 
 $OJ_CACHE_SHARE=false;
 require_once("oj-header.php");
 require_once("./include/db_info.inc.php");
require_once("./include/const.inc.php");
require_once("./include/my_func.inc.php");
?>

<link rel="stylesheet" href="http://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
<script src="http://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<?php // check user
$user=$_GET['user'];
if (!is_valid_user_name($user)){
	echo "No such User!";
	exit(0);
}
$user_mysql=mysql_real_escape_string($user);
$sql="SELECT `school`,`email`,`nick` FROM `users` WHERE `user_id`='$user_mysql'";
$result=mysql_query($sql);
$row_cnt=mysql_num_rows($result);
if ($row_cnt==0){ 
	echo "No such User!";
	exit(0);
}
$row=mysql_fetch_object($result);
$school=$row->school;
$email=$row->email;
$nick=$row->nick;
mysql_free_result($result);
$conteststr = "and problem_id NOT IN
(SELECT DISTINCT problem_id
FROM contest_problem, contest
WHERE contest_problem.contest_id = contest.contest_id
AND contest.duration !=0
AND contest.end_time >= NOW( ))" ;
// count solved
$sql="SELECT count(DISTINCT problem_id) as `ac` FROM `solution` WHERE `user_id`='".$user_mysql."' AND `result`=4 $conteststr";
$result=mysql_query($sql) or die(mysql_error());
$row=mysql_fetch_object($result);
$AC=$row->ac;
mysql_free_result($result);
// count submission
$sql="SELECT count(solution_id) as `Submit` FROM `solution` WHERE `user_id`='".$user_mysql."'";
$result=mysql_query($sql) or die(mysql_error());
$row=mysql_fetch_object($result);
$Submit=$row->Submit;
mysql_free_result($result);
// update solved 
$sql="UPDATE `users` SET `solved`='".strval($AC)."',`submit`='".strval($Submit)."' WHERE `user_id`='".$user_mysql."'";
$result=mysql_query($sql);
$sql="SELECT count(*) as `Rank` FROM `users` WHERE `solved`>$AC";
$result=mysql_query($sql);
$row=mysql_fetch_array($result);
$Rank=intval($row[0])+1;

/**
Array to control profile pages
**/
$pages_file = array("userinfo.php");
$pages_display = array("個人資料");
?>

<!-- Title -->
<h2>
	<?php echo htmlspecialchars($nick)?>
	<?php if($email) echo "<a href=mail.php?to_user=$user><i class='icono-comment'></i></a>";?>
<?php if($email) echo "<a href=addfriend.php?to_user=$user><i class='icono-heart'></i></a>";?>
</h2>
<p><i><?php echo "id = ".$user?></p></i>

<div style="overflow: hidden;">
<!--Page Menu -->
<div class="content-menu">
<?php
for($num = 0; $num < count($pages_file) ; $num++) {
    echo '<a href="javascript:Page(\''.$pages_file[$num].'\')" id="'.$pages_file[$num].'-btn" class="pure-button" style="width: 100%;">'.$pages_display[$num]."</a>\n";
}
?>
</div>

<!-- Page Content -->
<div class="content-pane">
<?php
	for($num = 0; $num < count($pages_file) ; $num++) {
		echo "<div style='display: none;' id='$pages_file[$num]'>\n";
		require_once("./user/profile_modules/".$pages_file[$num]);
		echo "</div>\n";
	}
?>
<script>
	var pages = ['<?php echo implode("','", $pages_file); ?>'];

	function clean(item){
		document.getElementById(item).style.display = "none";
		if ( document.getElementById(item+"-btn").classList.contains('button-primay') )
			document.getElementById(item+"-btn").classList.remove('button-primary');
	}
	function Page(pagename){
		pages.forEach(clean);
		document.getElementById(pagename).style.display = "block";
		document.getElementById(pagename+'-btn').classList.add('button-primary');
	}
	Page('<?php echo $pages_file[0]; ?>');
</script>
</div>
</div>
<?php require_once("oj-footer.php");?>
