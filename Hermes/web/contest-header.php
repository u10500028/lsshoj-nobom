<?php
require_once("config.php");
require_once('./include/cache_start.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSSH Online Judge</title>
    
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">
    <link rel="stylesheet" href="http://icono-49d6.kxcdn.com/icono.min.css">
	
    <link rel="stylesheet" href="<?php echo $OJ_BASE;?>include/menu.css">
    <link rel="stylesheet" href="<?php echo $OJ_BASE;?>include/theme.css">

</head>
<?php if(isset($_GET['cid']))
	$cid=intval($_GET['cid']);
if (isset($_GET['pid']))
	$pid=intval($_GET['pid']);
?>
<center>
<table class="menu-wrapper" width=100%><tr align=center>
	<td width=20%><a class="pure-menu-link menu-text" href='<?php echo $OJ_BASE;?>'>LSSHOJ</a>
	<td width=20%><a class="pure-menu-link menu-text" href='<?php echo $OJ_BASE;?>contest.php?cid=<?php echo $cid?>'><?php echo "問題"?></a>
	<td width=20%><a class="pure-menu-link menu-text" href='<?php echo $OJ_BASE;?>contestrank.php?cid=<?php echo $cid?>'><?php echo "名次"?></a>
	<td width=20%><a class="pure-menu-link menu-text" href='<?php echo $OJ_BASE;?>status.php?type=contest&cid=<?php echo $cid?>'><?php echo "狀態"?></a>
	<td width=20%><a class="pure-menu-link menu-text" href='<?php echo $OJ_BASE;?>conteststatistics.php?cid=<?php echo $cid?>'><?php echo "統計"?></a>
</tr></table>
</center>

<!--end menu-->
<?php
$contest_ok=true;
$str_private="SELECT count(*) FROM `contest` WHERE `contest_id`='$cid' && `private`='1'";
$result=mysql_query($str_private);
$row=mysql_fetch_row($result);
mysql_free_result($result);
if ($row[0]=='1' && !isset($_SESSION['c'.$cid])) $contest_ok=false;
if (isset($_SESSION['administrator'])) $contest_ok=true;
?>
<div class="main" id="main">
